#!/bin/sh

echo "Installing Homebrew..."
if which brew 2>/dev/null 1>/dev/null; then
    echo "Homebrew already installed."
else
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

echo "Installing brewdler"
brew tap Homebrew/bundle

echo "Executing Brewdler..."

brew bundle

cd config

echo "Configuring stow..."
stow -D stow
stow -t ~ stow

echo "Restowing all apps..."
for dir in */
do
    echo Unstowing $dir
    stow -D $dir
    echo Restowing $dir
    stow -t ~ $dir
done

if [[ ! -d ~/.oh-my-zsh ]]; then
	echo "Installing Oh My ZSH"
	
	git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
fi

chsh -s /bin/zsh

# Use a modified version of the Solarized Dark theme by default in Terminal.app
TERM_PROFILE='frozendevil';
CURRENT_PROFILE="$(defaults read com.apple.terminal 'Default Window Settings')";
if [ "${CURRENT_PROFILE}" != "${TERM_PROFILE}" ]; then
	cd ..
	open "settings/${TERM_PROFILE}.terminal";
	sleep 1; # Wait a bit to make sure the theme is loaded
	defaults write com.apple.terminal 'Default Window Settings' -string "${TERM_PROFILE}";
	defaults write com.apple.terminal 'Startup Window Settings' -string "${TERM_PROFILE}";
	
	read -p "The Terminal theme was updated and Terminal needs to relaunch, press [Enter] key to continue..."
	killall "Terminal" > /dev/null 2>&1
fi;