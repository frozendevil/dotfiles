# Frozendevil's dotfiles
A set of semi-reasonable config and settings files for use on a home computron.

##.gitconfg
A set of useful aliases and settings for git. Mostly based on those provided by [git-immersion](http://gitimmersion.com/lab_11.html#main_content), 
with the addition of 'a' for 'add' and 'rb' for 'rebase'. Also sets 'autosetuprebase' to true.

##.zshrc
Turns off oh-my-zsh provided themes, enables the git, svn and brew oh-my-zsh plugins and sets some more awesome stuff.

##frozendevil.terminal
Terminal settings based on [Tomorrow](https://github.com/ChrisKempson/Tomorrow-Theme) by ChrsKempson.