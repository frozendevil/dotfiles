# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Uncomment following line if you want to disable autosetting terminal title.
export DISABLE_AUTO_TITLE="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git svn brew)

source $ZSH/oh-my-zsh.sh

alias fixow='/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister -kill -r -domain local -domain user;killall Finder;echo "Open With has been rebuilt, Finder will relaunch"'

# Customize to your needs...
export GOPATH=~/Code/go
export PATH=~/bin:~/Library/Haskell/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:$GOPATH/bin
export RUST_SRC_PATH=~/Code/rust

code () {
    VSCODE_CWD="$PWD" open -n -b "com.microsoft.VSCode" --args $*
}

#prompt skinning below here

autoload -Uz vcs_info
autoload -U colors && colors
autoload -U promptinit

local reset="%{${reset_color}%}"
local white="%{$fg[white]%}"
local gray="%{$fg_bold[gray]%}"
local green="%{$fg_bold[green]%}"
local red="%{$fg[red]%}"
local yellow="%{$fg[yellow]%}"
local cyan="%{$fg[cyan]%}"
local blue="%{$fg[blue]%}"
local magenta="%{$fg[magenta]%}"

user_color=${cyan}

if [ "$UID" -eq "0" ]
then
       user_color=${red}
fi
export PROMPT="[${user_color}%n${reset}@${magenta}%m${reset}::${cyan}%~${reset}]$ "

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*:*' get-revision true
zstyle ':vcs_info:git*:*' check-for-changes true

# hash changes branch misc
zstyle ':vcs_info:git*' formats "%c%u %b%m"
zstyle ':vcs_info:git*' actionformats "(${yellow}%a${reset}) %c%u %b%m"
zstyle ':vcs_info:git*:*' stagedstr "${green}✪${reset}"
zstyle ':vcs_info:git*:*' unstagedstr "${yellow}⚠${reset}"

zstyle ':vcs_info:git*+set-message:*' hooks git-st git-stash

precmd () {
	vcs_info
	export RPROMPT="${vcs_info_msg_0_}"
}

function +vi-git-st() {
    local ahead behind remote
    local -a gitstatus

    # Are we on a remote-tracking branch?
    remote=${$(git rev-parse --verify ${hook_com[branch]}@{upstream} \
        --symbolic-full-name 2>/dev/null)/refs\/remotes\/}

    if [[ -n ${remote} ]] ; then
		hook_com[branch]="(${red}${hook_com[branch]}${reset}) [${green}${remote}${reset}]"
	else
		hook_com[branch]="(${red}${hook_com[branch]}${reset})"
    fi
}
